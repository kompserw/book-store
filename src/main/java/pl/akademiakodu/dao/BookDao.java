package pl.akademiakodu.dao;

import pl.akademiakodu.model.Book;

import java.util.List;

public interface BookDao {
    void save(Book b);
    List<Book> getList();
    Book find(int id);
    void remove(int id);
    void update(Book b);
    List<Book> findByQuery(String query);
}
