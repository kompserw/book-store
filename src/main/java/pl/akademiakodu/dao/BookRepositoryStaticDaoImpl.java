package pl.akademiakodu.dao;

import org.springframework.stereotype.Repository;
import pl.akademiakodu.model.Book;

import java.util.*;

@Repository
public class BookRepositoryStaticDaoImpl implements BookDao {

    private static Map<String, Book> map = new HashMap<>();

    @Override
    public void save(Book b) {
        map.put(UUID.randomUUID().toString(), b);
    }

    @Override
    public List<Book> getList() {
        return new ArrayList<>(map.values());
    }

    @Override
    public Book find(int id) {
        return null;
    }

    @Override
    public void remove(int id) {

    }

    @Override
    public void update(Book b) {

    }

    @Override
    public List<Book> findByQuery(String query) {
        return null;
    }
}
