package pl.akademiakodu.dao;

import pl.akademiakodu.model.Genre;

import java.util.List;

public interface GenreDao {
    List<Genre> findAll();

    Genre find(Long id);
}
