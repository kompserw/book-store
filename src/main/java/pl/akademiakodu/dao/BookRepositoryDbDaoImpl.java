package pl.akademiakodu.dao;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import pl.akademiakodu.model.Book;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Primary
@Transactional
public class BookRepositoryDbDaoImpl implements BookDao {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public void save(Book b) {
        entityManager.persist(b);
    }

    @Override
    public void update(Book b) {
        entityManager.merge(b);
    }

    @Override
    public List<Book> findByQuery(String queryString) {
        Query dbQuery = entityManager.createQuery("SELECT b FROM Book b WHERE LOWER(b.title) LIKE :title OR b.isbn.code = :isbn");
        dbQuery.setParameter("title", '%' + queryString.toLowerCase() + '%');
        dbQuery.setParameter("isbn", queryString);

        List<Book> resultList = (List<Book>) dbQuery.getResultList();
        return resultList;
    }

    @Override
    public List<Book> getList() {
        // HQL, not SQL
        Query query = entityManager.createQuery("FROM Book b");
        List<Book> resultList = (List<Book>) query.getResultList();
        return resultList;
    }

    @Override
    public Book find(int id) {
        return entityManager.find(Book.class, id);
    }

    @Override
    public void remove(int id) {
        Book book = find(id);
        entityManager.remove(book);
    }
}
