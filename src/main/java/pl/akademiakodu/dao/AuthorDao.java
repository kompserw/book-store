package pl.akademiakodu.dao;

import pl.akademiakodu.model.Author;

public interface AuthorDao {
    void save(Author author);
}
