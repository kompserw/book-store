package pl.akademiakodu.dao;

import pl.akademiakodu.model.ISBN;

public interface ISBNDao {
    void save(ISBN isbn);
    void update(ISBN isbn);
}
