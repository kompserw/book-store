package pl.akademiakodu.dao;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import pl.akademiakodu.model.Genre;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Primary
@Repository
public class GenreDaoImpl implements GenreDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @SuppressWarnings("unchecked")
    public List<Genre> findAll() {
        return (List<Genre>) entityManager.createQuery("FROM Genre g").getResultList();
    }

    @Override
    public Genre find(Long id) {
        return entityManager.find(Genre.class, id);
    }
}
