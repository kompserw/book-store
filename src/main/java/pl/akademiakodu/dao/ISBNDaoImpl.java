package pl.akademiakodu.dao;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.akademiakodu.model.ISBN;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
@Primary
@Transactional
public class ISBNDaoImpl implements ISBNDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(ISBN isbn) {
        entityManager.persist(isbn);
    }

    @Override
    public void update(ISBN isbn) {
        entityManager.merge(isbn);
    }
}
