package pl.akademiakodu.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.akademiakodu.dao.ISBNDao;
import pl.akademiakodu.model.ISBN;

@Service
public class ISBNServiceImpl implements ISBNService {

    @Autowired
    private ISBNDao isbnDao;

    @Override
    public void save(ISBN isbn) {
        if (isbn.getId() != null && isbn.getId() != 0) {
            isbnDao.save(isbn);
        } else {
            isbnDao.update(isbn);
        }
    }
}
