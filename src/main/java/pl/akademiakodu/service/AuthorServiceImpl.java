package pl.akademiakodu.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.akademiakodu.dao.AuthorDao;
import pl.akademiakodu.model.Author;

@Service
public class AuthorServiceImpl implements AuthorService {

    @Autowired
    AuthorDao authorDao;

    @Override
    public void save(Author author) {
        authorDao.save(author);
    }
}
