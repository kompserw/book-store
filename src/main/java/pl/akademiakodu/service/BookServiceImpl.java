package pl.akademiakodu.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.akademiakodu.dao.BookDao;
import pl.akademiakodu.model.Book;
import pl.akademiakodu.model.Genre;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private BookDao bookDao;

    @Autowired
    private GenreService genreService;

    @Autowired
    private ISBNService isbnService;

    @Override
    public void save(Book book) {
        Genre genre = genreService.find(book.getGenre().getId());
        book.setGenre(genre);
        //isbnService.save(book.getIsbn());
        // wyszukac po numerze ISBN ID -> ustawic ID dla ISBN
        // zapisac cala ksiazke

        if (book.getId() == 0) {
            bookDao.save(book);
        } else {
            bookDao.update(book);
        }
    }

    @Override
    public List<Book> getList() {
        return bookDao.getList();
    }

    @Override
    public Book read(int id) {
        return bookDao.find(id);
    }

    @Override
    public void remove(int id) {
        bookDao.remove(id);
    }

    @Override
    public void update(Book book) {

    }

    @Override
    public List<Book> findByQuery(String query) {
        return bookDao.findByQuery(query);
    }
}
