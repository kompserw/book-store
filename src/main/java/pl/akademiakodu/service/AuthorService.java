package pl.akademiakodu.service;

import pl.akademiakodu.model.Author;

public interface AuthorService {
    void save(Author author);
}
