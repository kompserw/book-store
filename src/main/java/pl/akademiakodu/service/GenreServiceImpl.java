package pl.akademiakodu.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.akademiakodu.dao.GenreDao;
import pl.akademiakodu.model.Genre;

import java.util.List;

@Service
public class GenreServiceImpl implements GenreService {

    @Autowired
    private GenreDao genreDao;

    @Override
    public List<Genre> listAll() {
        return genreDao.findAll();
    }

    @Override
    public Genre find(Long id) {
        return genreDao.find(id);
    }
}
