package pl.akademiakodu.service;

import pl.akademiakodu.model.ISBN;


public interface ISBNService {
    void save(ISBN isbn);
}
