package pl.akademiakodu.service;

import pl.akademiakodu.model.Book;

import java.util.List;

public interface BookService {
    void save(Book book);
    List<Book> getList();
    Book read(int id);
    void remove(int id);
    void update(Book book);
    List<Book> findByQuery(String query);
}
