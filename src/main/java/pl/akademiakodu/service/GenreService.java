package pl.akademiakodu.service;

import pl.akademiakodu.model.Genre;

import java.util.List;

public interface GenreService {
    List<Genre> listAll();

    Genre find(Long id);
}
