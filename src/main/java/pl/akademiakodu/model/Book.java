package pl.akademiakodu.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "BOOK")
public class Book {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private int id;

    @Column(name = "TITLE")
    @NotNull
    @NotEmpty(message = "Title cannot be empty")
    @Size(min = 5, message = "Title size cannot be less than 5")
    private String title;

    @Column(name = "PRICE")
    @Digits(integer = 4, fraction = 2, message = "Price must contain max. 4 integral numbers and 2 fraction")
    @Min(1)
    private double price;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "book")
    private List<Author> authors;

    @ManyToOne(cascade = CascadeType.REFRESH)
    private Genre genre;

    @OneToOne(cascade = CascadeType.ALL)
    private ISBN isbn;

    public Book() {

    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public ISBN getIsbn() {
        return isbn;
    }

    public void setIsbn(ISBN isbn) {
        this.isbn = isbn;
    }

    @Override
    public String toString() {
        return "pl.akademiakodu.model.Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", price=" + price +
                ", isbn=" + isbn +
                '}';
    }
}
