package pl.akademiakodu.model;

import javax.persistence.*;

@Table(name = "ISBN")
@Entity
public class ISBN {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;

    @Column(name = "CODE")
    private String code;

    @OneToOne(mappedBy = "isbn", cascade = CascadeType.ALL)
    private Book book;

    public ISBN() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "pl.akademiakodu.model.ISBN{" +
                "id=" + id +
                ", code='" + code + '\'' +
                '}';
    }
}
