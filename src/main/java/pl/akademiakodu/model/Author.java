package pl.akademiakodu.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Table(name = "AUTHOR")
public class Author {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    @NotEmpty(message = "Imię autora nie może być puste")
    @Size(min = 3, message = "Wymagane minimum 3 znaki")
    @Pattern(regexp = "[a-zA-ZąężźćóńśłĄĘŻŹĆÓŃŚŁ]+", message = "Nazwa musi zawierać tylko litery")
    @NotNull
    private String name;

    @Column(name = "SURNAME")
    @NotEmpty(message = "Nazwisko autora nie może być puste")
    @NotNull
    @Size(min = 3, message = "Wymagane minimum 3 znaki")
    @Pattern(regexp = "[a-zA-ZąężźćóńśłĄĘŻŹĆÓŃŚŁ]+", message = "Nazwa musi zawierać tylko litery")
    private String surname;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "BOOK_ID")
    private Book book;

    public Author() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    @Override
    public String toString() {
        return "pl.akademiakodu.model.Author{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", book=" + book +
                '}';
    }
}
