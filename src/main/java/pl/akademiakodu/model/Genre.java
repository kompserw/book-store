package pl.akademiakodu.model;

import javax.persistence.*;
import java.util.List;

@Table(name = "GENRE",
        uniqueConstraints = {
            @UniqueConstraint(name = "UC_NAME", columnNames = "NAME")
        })
@Entity
public class Genre {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @OneToMany(mappedBy = "genre")
    private List<Book> books;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    @Override
    public String toString() {
        return "pl.akademiakodu.model.Genre{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
