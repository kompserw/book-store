package pl.akademiakodu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.akademiakodu.model.Book;
import pl.akademiakodu.model.Genre;
import pl.akademiakodu.service.BookService;
import pl.akademiakodu.service.GenreService;

import javax.validation.Valid;

@Controller
@RequestMapping("/book")
public class BookController {

    @Autowired
    private BookService bookService;

    @Autowired
    private GenreService genreService;

    @GetMapping
    public ModelAndView getBooks() {
        ModelAndView mav = new ModelAndView("book-list");
        mav.addObject("list", bookService.getList());
        return mav;
    }

    @GetMapping("/add")
    public ModelAndView addBookForm() {
        ModelAndView mav = new ModelAndView("book-form");
        mav.addObject("book", new Book());
        mav.addObject("genres", genreService.listAll());
        return mav;
    }

    @GetMapping("/edit/{id}")
    public ModelAndView editBookForm(@PathVariable("id") int bookId) {
        ModelAndView mav = new ModelAndView("book-form");
        mav.addObject("book", bookService.read(bookId));
        mav.addObject("genres", genreService.listAll());
        return mav;
    }

    @PostMapping("/add")
    public ModelAndView saveBook(@Valid @ModelAttribute("book") Book book,
                                 BindingResult bindingResult) {
        System.out.println(book.getIsbn());
        ModelAndView mav = new ModelAndView();
        mav.addObject("genres", genreService.listAll());

        if (bindingResult.hasErrors()) {
            mav.addObject("book", book);
            mav.setViewName("book-form");
        } else {
            bookService.save(book);
            mav.setViewName("redirect:/book");
        }
        return mav;
    }

    @GetMapping("/{id}")
    public ModelAndView getDetails(@PathVariable("id") int bookId) {
        return new ModelAndView("book-details",
                "book", bookService.read(bookId));
    }

    @GetMapping("/remove/{id}")
    public String deleteBook(@PathVariable("id") int bookId) {
        bookService.remove(bookId);
        return "redirect:/book";
    }
}
