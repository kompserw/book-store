package pl.akademiakodu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.akademiakodu.model.Author;
import pl.akademiakodu.model.Book;
import pl.akademiakodu.service.AuthorService;
import pl.akademiakodu.service.BookService;

import javax.validation.Valid;

@Controller
@RequestMapping("/author")
public class AuthorController {

    @Autowired
    private BookService bookService;

    @Autowired
    private AuthorService authorService;

    @GetMapping("/add/{bookId}")
    public ModelAndView addAuthorFormForBook(@PathVariable("bookId") int bookId) {
        Book book = bookService.read(bookId);
        System.out.println("BOOK" + book);
        Author author = new Author();
        author.setBook(book);

        ModelAndView mav = new ModelAndView("author-form");
        mav.addObject("author", author);
        return mav;
    }

    @PostMapping("/add")
    public ModelAndView saveAuthorForBook(@Valid @ModelAttribute("author") Author author,
                                          BindingResult bindingResult) {
        int bookId = author.getBook().getId();
        Book book = bookService.read(bookId);
        author.setBook(book);

        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("author-form");
            mav.addObject("author", author);
            return mav;
        } else {
            System.out.println("author :: " + author);
            authorService.save(author);

            // redirect na listę książek
            return new ModelAndView("redirect:/book");
        }
    }
}
