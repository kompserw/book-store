INSERT INTO GENRE (id, name) VALUES (1, 'COMEDY');
INSERT INTO GENRE (id, name) VALUES (2, 'HORROR');
INSERT INTO GENRE (id, name) VALUES (3, 'DRAMA');
INSERT INTO GENRE (id, name) VALUES (4, 'THRILLER');

INSERT INTO ISBN (id, code) VALUES (1, '978-3-16-148410-6');
INSERT INTO ISBN (id, code) VALUES (2, '978-3-16-148410-5');
INSERT INTO ISBN (id, code) VALUES (3, '978-3-16-148410-4');
INSERT INTO ISBN (id, code) VALUES (4, '978-3-16-148410-3');
INSERT INTO ISBN (id, code) VALUES (5, '978-3-16-148410-1');
INSERT INTO ISBN (id, code) VALUES (6, '978-3-16-148410-2');
INSERT INTO ISBN (id, code) VALUES (7, '978-3-16-148410-7');
INSERT INTO ISBN (id, code) VALUES (8, '978-3-16-148410-8');
INSERT INTO ISBN (id, code) VALUES (9, '978-3-16-148410-9');
INSERT INTO ISBN (id, code) VALUES (10, '978-3-16-148410-0');


INSERT INTO BOOK (id, title, price, genre_id, isbn_id) VALUES (1, 'Z archiwum X', 129, 1, 1);
INSERT INTO BOOK (id, title, price, genre_id, isbn_id) VALUES (2, 'Pokolenie Z', 89, 2, 2);
INSERT INTO BOOK (id, title, price, genre_id, isbn_id) VALUES (3, 'Alicja w krainie czarów', 99, 3, 3);
INSERT INTO BOOK (id, title, price, genre_id, isbn_id) VALUES (4, 'Gwiezdne wojny', 99, 4, 4);
INSERT INTO BOOK (id, title, price, genre_id, isbn_id) VALUES (5, 'Opowieść wigilijna', 49, 3, 5);
INSERT INTO BOOK (id, title, price, genre_id, isbn_id) VALUES (6, 'Zaklinacz węży', 119, 2, 6);
INSERT INTO BOOK (id, title, price, genre_id, isbn_id) VALUES (7, 'Niebieskie niebo', 119, 1, 7);
INSERT INTO BOOK (id, title, price, genre_id, isbn_id) VALUES (8, 'Dom na Zanzibarze', 119, 2, 8);
INSERT INTO BOOK (id, title, price, genre_id, isbn_id) VALUES (9, 'Milczenie owiec', 119, 3, 9);
INSERT INTO BOOK (id, title, price, genre_id, isbn_id) VALUES (10, 'Epidemia', 219, 4, 10);

